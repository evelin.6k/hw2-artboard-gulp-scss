const gulp = require('gulp');
const clean = require('gulp-clean');
const browserSync = require('browser-sync').create();
const sass = require('gulp-sass')(require('sass'));
const rename = require('gulp-rename');
const cleanCSS = require('gulp-clean-css');
const uglify = require('gulp-uglify');
const pipeline = require('readable-stream').pipeline;
const imagemin = require('gulp-imagemin');
const autoprefixer = require('gulp-autoprefixer');

gulp.task('clean', function () {
    return gulp.src('dist/*', {read: false})
    .pipe(clean());
});

gulp.task('buildScss', function () {
    return gulp.src('./src/scss/style.scss')
    .pipe(sass())
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(autoprefixer({
        env: ['last 2 versions'],
        cascade: false
        }))
    .pipe(rename({suffix: '.min'}))
    .pipe(browserSync.stream())
    .pipe(gulp.dest('./dist/'));
});

gulp.task('buildJs', function () {
        return gulp.src('src/js/index.js')
        .pipe(uglify())
        .pipe(rename({suffix: '.min'}))
        .pipe(browserSync.stream())
        .pipe(gulp.dest('./dist/'))
});

gulp.task('imageMin', exports.default = () => (
        gulp.src('src/img/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/img'))
));

gulp.task('dev', function () {
    browserSync.init({
        server: { baseDir: './'}
    })
    gulp.watch('src/scss/*.scss', gulp.series(['buildScss']))
    gulp.watch('src/js/*.js', gulp.series(['buildJs']))
    gulp.watch('./index.html').on('change', () => browserSync.reload())
});

gulp.task('build', gulp.series('clean', gulp.parallel('buildScss', 'buildJs','imageMin')))