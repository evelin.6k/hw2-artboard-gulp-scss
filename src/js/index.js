let menuBtn = document.querySelector('.menu-button');
let menu = document.querySelector('.menu');
let cancelBtn = document.querySelector('.menu-button.button--cancel');

menuBtn.addEventListener('click', ()=> {
    menu.style.display = 'flex';
    cancelBtn.style.display = 'block';

})
cancelBtn.addEventListener('click',() => {
    menu.style.display = 'none';
    cancelBtn.style.display = 'none';

})